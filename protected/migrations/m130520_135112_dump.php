
<?php

/**
 * Created with https://github.com/schmunk42/database-command
 */

class m130520_135112_dump extends CDbMigration {

	public function safeUp() {
        if (Yii::app()->db->schema instanceof CMysqlSchema) {
           $options = 'ENGINE=InnoDB DEFAULT CHARSET=utf8';
        } else {
           $options = '';
        }

        // Schema for table 'interview'
        $this->createTable("interview", 
            array(
            "id"=>"int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
            "created"=>"datetime NOT NULL",
            "quest_id"=>"int(10) unsigned NOT NULL",
            "user"=>"varchar(200) NOT NULL",
            "data"=>"text NOT NULL",
            ), 
        $options);


        // Schema for table 'quest'
        $this->createTable("quest", 
            array(
            "id"=>"int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
            "name"=>"varchar(200) NOT NULL",
            ), 
        $options);


        // Schema for table 'question'
        $this->createTable("question", 
            array(
            "id"=>"int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT",
            "quest_id"=>"int(10) unsigned NOT NULL",
            "question"=>"text NOT NULL",
            "comment"=>"text NOT NULL",
            "order"=>"int(10) unsigned NOT NULL",
            "type"=>"varchar(20) NOT NULL",
            "data"=>"text NOT NULL",
            ), 
        $options);


        // Data for table 'quest'
        $this->insert("quest", array(
            "id"=>"1",
            "name"=>"Тестовая анкета номер 1",
        ) );

        $this->insert("quest", array(
            "id"=>"2",
            "name"=>"Тестовая анкета номер 2",
        ) );

        $this->insert("quest", array(
            "id"=>"3",
            "name"=>"Тестовая анкета без вопросов",
        ) );


        // Data for table 'question'
        $this->insert("question", array(
            "id"=>"1",
            "quest_id"=>"1",
            "question"=>"Заголовок раздела",
            "comment"=>"Комментарий к разделу",
            "order"=>"1",
            "type"=>"comment",
            "data"=>"",
        ) );

        $this->insert("question", array(
            "id"=>"2",
            "quest_id"=>"1",
            "question"=>"Текстовый вопрос",
            "comment"=>"",
            "order"=>"2",
            "type"=>"text",
            "data"=>"",
        ) );

        $this->insert("question", array(
            "id"=>"3",
            "quest_id"=>"1",
            "question"=>"Радиокнопки",
            "comment"=>"",
            "order"=>"3",
            "type"=>"radio",
            "data"=>"a:3:{i:0;s:16:\"Вариант 1\";i:1;s:16:\"Вариант 2\";i:2;s:16:\"Вариант 3\";}",
        ) );

        $this->insert("question", array(
            "id"=>"4",
            "quest_id"=>"1",
            "question"=>"Чекбоксы",
            "comment"=>"",
            "order"=>"4",
            "type"=>"check",
            "data"=>"a:3:{i:0;s:16:\"Вариант 1\";i:1;s:16:\"Вариант 2\";i:2;s:16:\"Вариант 3\";}",
        ) );

        $this->insert("question", array(
            "id"=>"5",
            "quest_id"=>"2",
            "question"=>"Вопрос",
            "comment"=>"Комментарий к вопросу",
            "order"=>"1",
            "type"=>"text",
            "data"=>"",
        ) );


	}

	public function safeDown() {
		echo 'Migration down not supported.';
	}

}

?>
