<?php

/**
 * HTTPBadRequest
 */
class Http400 extends CHttpException {
    function __construct($message = null, $code = 0) {
        parent::__construct('400', $message ?: 'Bad Request', $code);
    }
}