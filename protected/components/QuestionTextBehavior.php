<?php

/**
 * Вопрос с text
 */
class QuestionTextBehavior extends QuestionAbstractBehavior {

    public $defaultQuestion = 'Вопрос с text';

    public function testAnswer(&$answer) {
        if (empty($answer)) return 'Ответ не может быть пустым';
        return parent::testAnswer($answer);
    }
}