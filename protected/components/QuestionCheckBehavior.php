<?php

/**
 * Вопрос с checkboxes
 * Минимум один вариант возможных ответов
 */
class QuestionCheckBehavior extends QuestionAbstractBehavior {

    private $_answers = array();

    public $defaultQuestion = 'Вопрос с checkboxes';

    public function canGetProperty($name) {
        return in_array($name, ['answers', 'answers_a']) ;
    }

    public function canSetProperty($name) {
        return $this->canGetProperty($name);
    }

    public function __get($name) {
        return ($name == 'answers') ? implode("\n", $this->_answers) : $this->_answers;
    }

    public function __set($name, $value) {
        $this->_answers = ($name == 'answers') ? explode("\n", $value) : $value;
        $this->_answers = array_filter($this->_answers, function ($v) {return trim($v);});
    }

    public function beforeSave($event) {
        $this->getOwner()->data = serialize($this->_answers);
    }

    public function afterFind($event) {
        $this->_answers = unserialize($this->getOwner()->data);
        if (!is_array($this->_answers)) $this->_answers = array();
    }

    public function getFormElements() {
        return array(
            'answers' => array('name'=>'answers', 'type' => 'textarea', 'htmlOptions' => ['hint' => 'Через перевод строки']),
        );
    }

    public function rules() {
        return array(
            array('answers', 'validateEmptyArray', 'min'=>2),
            array('answers', 'required'),
        );
    }

    public function attributeLabels() {
        return array(
            'answers' => 'Ответы',
        );
    }

    public function testAnswer(&$answers) {
        if (empty($answers)) return 'Выберите хотя бы один ответ';
        if (!is_array($answers)) return 'Выберите хотя бы один ответ';
        if (count($answers) == 0) return 'Выберите хотя бы один ответ';
        foreach ($answers as $answer => $v) {
            if (!array_key_exists($answer, $this->_answers)) return 'Такого ответа не существует';
        }
        return parent::testAnswer($answers);
    }

    public function answerVerbose($answers) {
        $ret = [];
        foreach ($answers as $answer => $v) {
            if (array_key_exists($answer, $this->_answers)) $ret[] = trim($this->_answers[$answer]);
        }
        return implode('; ', $ret);
    }

}