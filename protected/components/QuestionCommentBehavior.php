<?php

/**
 * Пустой вопрос, он же комментарий
 */
class QuestionCommentBehavior extends QuestionAbstractBehavior {

    public $defaultQuestion = 'Комментарий';


    public function testAnswer(&$answer) {
        $answer = '';
        return parent::testAnswer($answer);
    }
}