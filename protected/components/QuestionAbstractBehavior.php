<?php

/**
 * класс абстрактный, чтоб жизнь мёдом не казалась
 *
 * @method Question getOwner()
 * @property Question $owner
 */

abstract class QuestionAbstractBehavior extends CActiveRecordBehavior {

    public $defaultQuestion = 'Вопрос';

    /**
     * распаковываются данные
     *
     * @param CModelEvent $event
     */
    public function afterConstruct($event) {
    }

    /**
     * запаковываются данные
     *
     * @param CModelEvent $event
     */
    public function beforeSave($event) {
    }

    /**
     * список элементов формы (не обязательно полей), добавляемых к общим
     *
     * @return array
     */
    public function getFormElements() {
        return array();
    }

    /**
     * список правил валидации, добавляемых к общим
     *
     * @return array
     */
    public function rules() {
        return array();
    }

    /**
     * список названий полей, добавляемых к общим
     *
     * @return array
     */
    public function attributeLabels() {
        return array();
    }

    /**
     * проверка ответа на ошибку
     *
     * @param $answer
     * @return mixed
     */
    public function testAnswer(&$answer) {
        return null;
    }

    /**
     * конвертация ответа в текст
     *
     * @param $answer
     * @return string
     */
    public function answerVerbose($answer) {
        return $answer;
    }

}