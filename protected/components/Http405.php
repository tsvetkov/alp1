<?php

/**
 * HTTPMethodNotAllowed
 */
class Http405 extends CHttpException {
    function __construct($message = null, $code = 0) {
        parent::__construct('405', $message ?: 'Method Not Allowed', $code);
    }
}