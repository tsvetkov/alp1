<?php

/**
 * HTTPForbidden
 */
class Http403 extends CHttpException {
    function __construct($message = null, $code = 0) {
        parent::__construct('403', $message ?: 'Forbidden', $code);
    }
}