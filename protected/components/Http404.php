<?php

/**
 * HTTPNotFound
 */
class Http404 extends CHttpException {
    function __construct($message = null, $code = 0) {
        parent::__construct('404', $message ?: 'Not Found', $code);
    }
}