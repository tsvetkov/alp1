<?php

/**
 * Вопрос с radiobuttons
 * Минимум два варианта возможных ответов
 */
class QuestionRadioBehavior extends QuestionAbstractBehavior {

    private $_answers = array();

    public $defaultQuestion = 'Вопрос с radiobuttons';

    public function canGetProperty($name) {
        return in_array($name, ['answers', 'answers_a']) ;
    }

    public function canSetProperty($name) {
        return $this->canGetProperty($name);
    }

    public function __get($name) {
        return ($name == 'answers') ? implode("\n", $this->_answers) : $this->_answers;
    }

    public function __set($name, $value) {
        $this->_answers = ($name == 'answers') ? explode("\n", $value) : $value;
        $this->_answers = array_filter($this->_answers, function ($v) {return trim($v);});
    }

    public function beforeSave($event) {
        $this->getOwner()->data = serialize($this->_answers);
    }

    public function afterFind($event) {
        $this->_answers = unserialize($this->getOwner()->data);
        if (!is_array($this->_answers)) $this->_answers = array();
    }

    public function getFormElements() {
        return array(
            'answers' => array('name'=>'answers', 'type' => 'textarea', 'htmlOptions' => ['hint' => 'Через перевод строки']),
        );
    }

    public function rules() {
        return array(
            array('answers', 'validateEmptyArray', 'min'=>2),
            array('answers', 'required'),
        );
    }

    public function attributeLabels() {
        return array(
            'answers' => 'Ответы',
        );
    }

    public function testAnswer(&$answer) {
        if (is_null($answer)) return 'Выберите вариант ответа';
        if (!array_key_exists($answer, $this->_answers)) return 'Такого ответа не существует';
        return parent::testAnswer($answer);
    }

    public function answerVerbose($answer) {
        return $this->_answers[$answer];
    }
}