<?php

/**
 * HTTPInternalServerError
 */
class Http500 extends CHttpException {
    function __construct($message = null, $code = 0) {
        parent::__construct('500', $message ?: 'Internal Server Error', $code);
    }
}