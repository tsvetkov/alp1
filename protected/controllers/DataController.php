<?php

class DataController extends Controller {

    public function actionIndex() {
        $this->breadcrumbs += array('Данные');
        $this->pageTitle = 'Данные';

        $data = new CActiveDataProvider('Interview', array(
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $this->render('index', array(
            'data' => $data,
        ));
    }

    public function actionView($id) {
        $this->breadcrumbs += array('Данные'=>'index');
        $this->breadcrumbs += array('Просмотр анкеты');
        $this->pageTitle = 'Просмотр анкеты';

        /** @var $model Interview */
        $model = Interview::model()->findByPk($id);
        if (!$model) throw new Http404();

        $this->render('view', array(
            'interview' => $model,
            'data' => $model->data
        ));
    }

    public function actionDelete($id) {
        /** @var $model Interview */
        $model = Interview::model()->findByPk($id);
        if (!$model) throw new Http404();

        $model->delete();
        $this->redirect(['index']);
    }
}
