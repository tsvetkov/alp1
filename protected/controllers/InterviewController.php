<?php

class InterviewController extends Controller {

    public function actionIndex() {
        $this->breadcrumbs += array('Тестирование');
        $this->pageTitle = 'Тестирование';

        $quests = Quest::model()->findAll();

        $this->render('index', array(
            'quests' => $quests,
        ));
    }

    public function actionQuest($qid) {
        /** @var $quest Quest */
        $quest = Quest::model()->findByPk($qid);
        if (!$quest) throw new Http404();

        $this->breadcrumbs += array('Тестирование'=>'/interview/index');
        $this->breadcrumbs += [$quest->name];
        $this->pageTitle = $quest->name;

        $questions = $quest->questions;

        $this->render('quest', array(
            'quest' => $quest,
            'questions' => $questions,
        ));
    }

    public function actionAnswer($qid) {
        /** @var $quest Quest */
        $quest = Quest::model()->findByPk($qid);
        if (!$quest) throw new Http404();

        $this->breadcrumbs += array('Тестирование'=>'/interview/index');
        $this->breadcrumbs += [$quest->name];
        $this->pageTitle = $quest->name;

        if (!Yii::app()->request->isPostRequest) throw new Http404();

        $questions = $quest->questions;
        $answers = array_key_exists('answer', $_POST) ? $_POST['answer'] : [];
        $data = [];
        $errors = [];

        foreach ($questions as $question) {
            if (!$question->isQuestion()) continue;
            $error = $question->testAnswer($answers[$question->order]);
            if ($error) {
                $errors[$question->order] = $error;
            } else {
                $data[$question->order] = [
                    'question' => $question->question,
                    'answer' => $question->answerVerbose($answers[$question->order]),
                    'question_id' => $question->id,
                ];
            }
        }

        $user = array_key_exists('user', $_POST) ? $_POST['user'] : '';
        if (!$user) $errors['user'] = 'Имя пользователя должно быть указано';
        $answers['user'] = $user;

        if (empty($errors) && count($data) > 0) {
            $m = new Interview();
            $m->data = $data;
            $m->quest = $quest;
            $m->user = $user;
            $m->save();
            if (!$m->id) throw new Http500();
            $this->redirect(['final', 'id'=>$m->id]);
        }

        $this->render('quest', array(
            'quest' => $quest,
            'questions' => $questions,
            'errors' => $errors,
            'currents' => $answers
        ));
    }

    public function actionFinal($id) {
        /** @var $model Interview */
        $model = Interview::model()->findByPk($id);
        if (!$model) throw new Http404();

        $quest = $model->quest;

        $this->breadcrumbs += array('Тестирование'=>'/interview/index');
        $this->breadcrumbs += [$quest->name];
        $this->pageTitle = 'Окончание тестирования';

        $this->render('final', array(
            'quest' => $quest,
            'interview' => $model,
        ));
    }

}
