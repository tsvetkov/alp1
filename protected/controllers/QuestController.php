<?php

class QuestController extends Controller {

    public function actionIndex() {
        $this->breadcrumbs += array('Тесты');
        $this->pageTitle = 'Тесты';

        $data = new CActiveDataProvider('Quest', array(
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $this->render('index', array(
            'data' => $data,
        ));
    }

    public function actionAdd() {
        $this->breadcrumbs += ['Тесты'=>'/quest/index'];
        $this->breadcrumbs += ['Новый тест'];
        $this->pageTitle = 'Новый тест';

        /** @var $model Quest */
        $model = new Quest();

        $form = $model->getForm();
        if ($form->submitted() && $form->validate()) {
            $model->save();
            if (!$model->id) throw new Http500();
            $this->redirect(['/quest/index']);
        }
        $this->render('add', array(
            'model' => $model,
            'form' => $form,
        ));
    }

    public function actionEdit($id) {
        $this->breadcrumbs += ['Тесты'=>'/quest/index'];
        $this->breadcrumbs += ['Изменение теста'];
        $this->pageTitle = 'Изменение теста';

        /** @var $model Quest */
        $model = Quest::model()->findByPk($id);
        if (!$model) throw new Http404();

        $form = $model->getForm();
        if ($form->submitted() && $form->validate()) {
            $model->save();
            if (!$model->id) throw new Http500();
            $this->redirect(['/quest/index']);
        }
        $this->render('edit', array(
            'model' => $model,
            'form'  => $form,
        ));
    }


    public function actionView($id) {
        /** @var $model Quest */
        $model = Quest::model()->findByPk($id);
        if (!$model) throw new Http404();

        $this->breadcrumbs += ['Тесты'=>'/quest/index'];
        $this->breadcrumbs += [$model->name];
        $this->pageTitle = $model->name;

        $data = new CActiveDataProvider('Question', array(
            'criteria' => array(
                'with' => array('quest'),
                'condition' => 'quest_id='.$model->id,
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $this->render('view', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionDelete($id) {
        /** @var $model Quest */
        $model = Quest::model()->findByPk($id);
        if (!$model) throw new Http404();

        $model->delete();
        $this->redirect(['/quest/index']);
    }
}
