<?php

class QuestionController extends Controller {

    public function actionIndex() {
        $this->redirect(['/quest']);
    }

    public function actionAdd($qid, $type) {
        /** @var $quest Quest */
        $quest = Quest::model()->findByPk($qid);
        if (!$quest) throw new Http404();

        $this->breadcrumbs += ['Тесты'=>'/quest/index'];
        $this->breadcrumbs += [$quest->name => $this->createUrl('/quest/view', ['id' => $quest->id])];
        $this->breadcrumbs += ['Новый вопрос'];
        $this->pageTitle = 'Новый вопрос';

        /** @var $model Question */
        $model = new Question();
        $model->setType($type);
        $model->quest = $quest;

        $form = $model->getForm();
        if ($form->submitted() && $form->validate()) {
            $model->save();
            if (!$model->id) throw new Http500();
            $this->redirect(['quest/view', 'id'=>$quest->id]);
        }
        $this->render('add', array(
            'model' => $model,
            'form' => $form,
        ));
    }

    public function actionEdit($id) {
        /** @var $model Question */
        $model = Question::model()->findByPk($id);
        if (!$model) throw new Http404();

        $quest = $model->quest;

        $this->breadcrumbs += ['Тесты'=>'/quest/index'];
        $this->breadcrumbs += [$quest->name => $this->createUrl('/quest/view', ['id' => $quest->id])];
        $this->breadcrumbs += [$model->question];
        $this->pageTitle = 'Изменение вопроса';

        $form = $model->getForm();
        if ($form->submitted() && $form->validate()) {
            $model->save();
            if (!$model->id) throw new Http500();
            $this->redirect(['quest/view', 'id'=>$quest->id]);
        }
        $this->render('edit', array(
            'model' => $model,
            'form'  => $form,
        ));
    }


    public function actionView($id) {
        /** @var $model Question */
        $model = Question::model()->findByPk($id);
        if (!$model) throw new Http404();

        $quest = $model->quest;

        $this->breadcrumbs += ['Тесты'=>'/quest/index'];
        $this->breadcrumbs += [$quest->name => $this->createUrl('/quest/view', ['id' => $quest->id])];
        $this->breadcrumbs += [$model->question];
        $this->pageTitle = $model->question;

        $this->render('view', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        /** @var $model Question */
        $model = Question::model()->findByPk($id);
        if (!$model) throw new Http404();

        $quest = $model->quest;

        $model->delete();
        $this->redirect(['quest/view', 'id'=>$quest->id]);
    }
}
