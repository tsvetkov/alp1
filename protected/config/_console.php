<?php

//Yii::setPathOfAlias('database-command', dirname(__FILE__) . '/../../vendor/schmunk42/database-command');

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return [
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => 'Console',

    // preloading 'log' component
    'preload'    => ['log'],

    // application components
    'components' => [
        'log' => [
            'class'  => 'CLogRouter',
            'routes' => [
                [
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ],
            ],
        ],
    ],

//    'commandMap' => [
//        'database' => [
//            'class' => 'database-command.EDatabaseCommand',
//        ],
//    ],
];