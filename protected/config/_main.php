<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('twig', dirname(__FILE__) . '/../../vendor/twig/twig/lib/Twig');
Yii::setPathOfAlias('twig-renderer', dirname(__FILE__) . '/../../vendor/yiiext/twig-renderer');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../../vendor/crisu83/yii-bootstrap');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => 'alp.test1',
    'theme'      => 'bootstrap',

    // preloading 'log' component
    'preload'    => ['log'],

    // autoloading model and component classes
    'import'     => [
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
    ],

    'modules'    => [
        // uncomment the following to enable the Gii tool
        'gii' => [
            'class'          => 'system.gii.GiiModule',
            'password'       => '',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'      => ['127.0.0.1', '::1'],
            'generatorPaths' => [
                'bootstrap.gii',
            ],
        ],
    ],

    // application components
    'components' => [
        'clientScript' => [
            'scriptMap' => [
                'jquery.js'=>'http://code.jquery.com/jquery-latest.min.js',
                'jquery.min.js'=>'http://code.jquery.com/jquery-latest.min.js',
            ]
        ],
        'bootstrap'    => [
            'class' => 'bootstrap.components.Bootstrap',
        ],
        'viewRenderer' => [
            'class' => 'twig-renderer.ETwigViewRenderer',
            'twigPathAlias' => 'twig',
            // Все параметры ниже являются необязательными
            'fileExtension' => '.twig',
            'options' => [
                'autoescape' => true,
            ],
            //'extensions' => [
            //    'My_Twig_Extension',
            //],
            'globals' => [
                'html' => 'CHtml',
                'Yii' => 'Yii'
            ],
            'functions' => [
                'rot13' => 'str_rot13',
            ],
            'filters' => [
                'jencode' => 'CJSON::encode',
            ],
            // Пример изменения синтаксиса на Smarty-подобный (не рекомендуется использовать)
            //'lexerOptions' => array(
            //    'tag_comment'  => array('{*', '*}'),
            //    'tag_block'    => array('{', '}'),
            //    'tag_variable' => array('{$', '}')
            //),
        ],
        'user'         => [
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ],
        // uncomment the following to enable URLs in path-format
        'urlManager'   => [
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules'     => [
                '<controller:\w+>/<id:\d+>'              => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
            ],
        ],
        'errorHandler' => [
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ],
        'log'          => [
            'class'  => 'CLogRouter',
            'routes' => [
                [
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ],
                // uncomment the following to show log messages on web pages
                /*
                [
                    'class'=>'CWebLogRoute',
                ],
                */
            ],
        ],
    ],

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'     => [
        // this is used in contact page
        'adminEmail' => 'root@brixy.ru',
    ],
];

