<?php

/**
 * Интерьвю
 *
 * @property int $id автоинкремент
 * @property string $name
 * @property Question[] $questions
 * @property Interview[] $interviews
 * @method EForm getForm()
 */
class Quest extends CActiveRecord {

    /**
     * @param string $className
     * @return Quest|CActiveRecord
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function tableName() {
        return 'quest';
    }

    /**
     * @return array
     */
    public function behaviors() {
        return [
            'form' => ['class' => 'EFormModelBehavior',],
            'relate' => ['class' => 'EAdvancedArBehavior',]
        ];
    }


    /**
     * @return array
     */
    public function rules() {
        return [
            ['name', 'required']
        ];
    }

    /**
     * @return array
     */
    public function relations() {
        return [
            'questions' => [self::HAS_MANY, 'Question', 'quest_id'],
            'interviews' => [self::HAS_MANY, 'Interview', 'quest_id'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'questions' => 'Вопросы',
            'interviews' => 'Ответы',
        );
    }

    /**
     * @return array
     */
    public function defaultScope() {
        return [
            'order'=>"name asc",
        ];
    }

    public function getLink() {
        return CHtml::link($this->name, array('/quest/view', 'id'=>$this->id));
    }

}