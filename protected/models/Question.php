<?php

/**
 * Вопросы
 *
 * @property int $id автоинкремент
 * @property int $order порядок вопросов
 * @property Quest $quest анкета
 * @property string $question текст вопроса
 * @property string $comment комментарий к вопросу
 * @property string $type тип вопроса
 * @property string $data доп.данные (содержимое определяется типом вопроса)
 * @method EForm getForm()
 * @property QuestionAbstractBehavior $_type
 */
class Question extends CActiveRecord {

    const TYPE_DEFAULT = 'comment';
    static private $TYPES = array(
        'comment' => 'QuestionCommentBehavior', /** @see QuestionCommentBehavior */
        'text'    => 'QuestionTextBehavior', /** @see QuestionTextBehavior */
        'radio'   => 'QuestionRadioBehavior', /** @see QuestionRadioBehavior */
        'check'   => 'QuestionCheckBehavior', /** @see QuestionCheckBehavior */
    );

    private $_old_order = 0;

    /**
     * @param string $className
     * @return Question|CActiveRecord
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function tableName() {
        return 'question';
    }

    /**
     * @return array
     */
    public function behaviors() {
        return [
            'form' => ['class' => 'EFormModelBehavior',],
            'relate' => ['class' => 'EAdvancedArBehavior',]
        ];
    }

    /**
     * @return array
     */
    public function rules() {
        return array_merge(array(
            array('type', 'required'),
            array('question', 'length', 'max' => 5000),
            array('comment', 'length', 'max' => 5000),
            array('question', 'required'),
        ), $this->_type->rules());
    }

    /**
     * @return array
     */
    public function getFormElements() {
        return array_merge(array(
//            'type' => array('type' => 'dropdownlist', 'data' => $this->getTypesVerbose(), 'class'=>"input2-select"),
            'question' => array('type' => 'text', 'class'=>"input2-text"),
            'comment' => array('type' => 'textarea', 'class'=>"input2-textarea"),
        ), $this->_type->getFormElements());
    }

    /**
     * @return array
     */
    public function relations() {
        return [
            'quest'    => array(self::BELONGS_TO, 'Quest', 'quest_id'),
        ];
    }

    /**
     * @return array
     */
    public function defaultScope() {
        return [
            'order'=>"`order` asc",
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return array_merge(array(
            'id' => 'ID',
            'order' => 'Порядок',
            'type' => 'Тип вопроса',
            'quest' => 'Анкета',
            'question' => 'Вопрос',
            'comment' => 'Комментарий',
        ), isset($this->_type) ? $this->_type->attributeLabels() : array());
    }

    public function afterConstruct() {
        $this->type = self::TYPE_DEFAULT; # потому что без поведения будет печально
        $this->attachBehavior('_type', self::$TYPES[$this->type]);
        parent::afterConstruct();
    }

    public function afterFind() {
        $this->attachBehavior('_type', self::$TYPES[$this->type]);
        $this->_old_order = $this->order;
        parent::afterFind();
    }

    /**
     * после сохранения из жадности переинициализируется тип
     */
    protected function afterSave() {
        $this->detachBehavior('_type');
        $this->attachBehavior('_type', self::$TYPES[$this->type]);
        parent::afterSave();
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            /** @var $last self */
            $last = self::model()->find(array(
                'condition' => 'quest_id='.$this->quest->id,
                'order' => '`order` DESC',
                'limit' => 1
            ));
            $this->order = $last ? $last->order + 1 : 1;
            if (!$this->question) {
                $this->detachBehavior('_type');
                $this->attachBehavior('_type', self::$TYPES[$this->type]);
                $this->question = $this->_type->defaultQuestion;
            }
        }
        return parent::beforeSave();
    }

    public function delete() {
        $following_records = self::model()->findAll(array(
            'order' => '`order` ASC',
            'condition' => '`order` > '.$this->order . ' and ' . 'quest_id='.$this->quest->id,
        ));
        /** @var $record self */
        foreach ($following_records as $record) {
            $record->order--;
            $record->update();
        }
        return parent::delete();
    }

    public function getLink() {
        return CHtml::link($this->question, array('/question/view', 'id'=>$this->id));
    }

    public function setType($type) {
        if ($this->type != $type) {
            $this->type = $type;
            $this->detachBehavior('_type');
            $this->attachBehavior('_type', self::$TYPES[$this->type]);
        }
    }

    public function getTypeVerbose() {
        return $this->_type->defaultQuestion;
    }

    /**
     * Валидаторы для поведений (их конечно можно вынести в компоненты и переиспользовать, но вообще-то они нигде не переиспользуются)
     *
     * @param $attribute
     * @param $params
     */
    public function validateEmptyArray($attribute, $params) {
        $min = !empty($params['min']) && (int)$params['min'] > 1 ? (int)$params['min'] : 1;
        $a = $this->$attribute;
        if (!is_array($a)) $a = explode("\n", $a);
        $a = array_filter($a, function ($v) {return trim($v);});
        if (count($a) == 0) {
            $this->addError($attribute, 'Список не может быть пустым');
        } elseif (count($a) < $min) {
            $this->addError($attribute, 'Требуется как минимум '.$min.' элемент(а)');
        }
    }

    public function testAnswer(&$answer) {
        return $this->_type->testAnswer($answer);
    }

    public function answerVerbose(&$answer) {
        return $this->_type->answerVerbose($answer);
    }

    public function isQuestion() {
        return ($this->type !== self::TYPE_DEFAULT);
    }

    private function getTypesVerbose() {
        static $types = [];
        if (!count($types)) {
            $types = self::$TYPES;
            array_walk($types, function(&$v, $k) {
                /** @var $c QuestionAbstractBehavior */
                $c = new $v();
                $v = $c->defaultQuestion;
            });
        }
        return $types;
    }


}