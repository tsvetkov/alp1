<?php

/**
 * Интерьвю
 *
 * @property int $id автоинкремент
 * @property string $user имя анкетируемого
 * @property string $data ответы
 * @property Quest $quest анкета
 * @property datetime $created дата создания
 */
class Interview extends CActiveRecord {

    /**
     * @param string $className
     * @return Interview|CActiveRecord
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function tableName() {
        return 'interview';
    }

    /**
     * @return array
     */
    public function behaviors() {
        return [
            'form' => ['class' => 'EFormModelBehavior',],
            'relate' => ['class' => 'EAdvancedArBehavior',]
        ];
    }


    /**
     * @return array
     */
    public function rules() {
        return [];
    }

    /**
     * @return array
     */
    public function relations() {
        return [
            'quest'    => array(self::BELONGS_TO, 'Quest', 'quest_id'),
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'created' => 'Дата создания',
            'user' => 'Имя анкетируемого',
            'quest' => 'Анкета',
            'data' => 'Ответы',
        );
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->created = new CDbExpression('NOW()');
        }
        if (is_array($this->data)) $this->data = serialize($this->data);
        return parent::beforeSave();
    }

    protected function afterFind() {
        if (!is_array($this->data)) $this->data = unserialize($this->data);
        parent::afterFind();
    }

    public function getLink() {
        return CHtml::link($this->created, array('/data/view', 'id'=>$this->id));
    }

}